﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DentaProv1
{
    public partial class ÄndraVy : Form
    {
        public ÄndraVy()
        {
            InitializeComponent();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            FirstSide fSide = new FirstSide();
            fSide.Show();
            this.Hide();
        }

        private void ÄndraVy_FormClosed(object sender, FormClosedEventArgs e)
        {
            Tandläkare tand = new Tandläkare();
            tand.Show();
        }
    }
}
