﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;

namespace DentaProv1
{
    public partial class FirstSide : Form
    {
        public FirstSide()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {

        }



        private void txtAnvändarNamn_TextChanged(object sender, EventArgs e)
        {
            
        }
        private void txtLösenord_TextChanged(object sender, EventArgs e)
        {
            txtLösenord.PasswordChar = '*';
        }


        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void FirstSide_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

       
        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            txtAnvändarNamn.Clear();
            txtLösenord.Clear();
            txtAnvändarNamn.Focus();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnLoggaIn_Click(object sender, EventArgs e)
        {
            if ((txtAnvändarNamn.Text.Contains("rec")) && (txtLösenord.Text.Contains("rec")))
            {
                ReceptionSide recep = new ReceptionSide();
                recep.Show();
                recep.label3.Text = txtAnvändarNamn.Text;
                this.Hide();
            }
            else if ((txtAnvändarNamn.Text.Contains("tdl")) && (txtLösenord.Text.Contains("tdl")))
            {
                Tandläkare läkare = new Tandläkare();
                läkare.Show();
                läkare.label3.Text = txtAnvändarNamn.Text;
                this.Hide();
            }
            else if ((txtAnvändarNamn.Text.Contains("admin")) && (txtLösenord.Text.Contains("admin")))
            {
                MessageBox.Show("Here goes the code for the admin!");
            }
            else
            {
                MessageBox.Show("UserName or Password wrong! Try again!");
                txtAnvändarNamn.Clear();
                txtLösenord.Clear();
                txtAnvändarNamn.Focus();
            }
        }

        private void panel3_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void txtAnvändarNamn_MouseClick(object sender, MouseEventArgs e)
        {
            txtAnvändarNamn.Text = string.Empty;
        }

        private void txtLösenord_MouseClick(object sender, MouseEventArgs e)
        {
            txtLösenord.Text = string.Empty;
        }
    }
}
