﻿namespace DentaProv1
{
    partial class Email
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSkicka = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTill = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtRubruk = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMeddelande = new System.Windows.Forms.TextBox();
            this.webBrowser = new System.Windows.Forms.WebBrowser();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.Rubrik = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Avsändare = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnReceive = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSkicka
            // 
            this.btnSkicka.Location = new System.Drawing.Point(1091, 324);
            this.btnSkicka.Name = "btnSkicka";
            this.btnSkicka.Size = new System.Drawing.Size(93, 23);
            this.btnSkicka.TabIndex = 0;
            this.btnSkicka.Text = "Skicka";
            this.btnSkicka.UseVisualStyleBackColor = true;
            this.btnSkicka.Click += new System.EventHandler(this.btnSkicka_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Till:";
            // 
            // txtTill
            // 
            this.txtTill.Location = new System.Drawing.Point(122, 54);
            this.txtTill.Name = "txtTill";
            this.txtTill.Size = new System.Drawing.Size(1062, 22);
            this.txtTill.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Rubrik: ";
            // 
            // txtRubruk
            // 
            this.txtRubruk.Location = new System.Drawing.Point(122, 93);
            this.txtRubruk.Name = "txtRubruk";
            this.txtRubruk.Size = new System.Drawing.Size(1062, 22);
            this.txtRubruk.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Meddelande:";
            // 
            // txtMeddelande
            // 
            this.txtMeddelande.Location = new System.Drawing.Point(122, 132);
            this.txtMeddelande.Multiline = true;
            this.txtMeddelande.Name = "txtMeddelande";
            this.txtMeddelande.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtMeddelande.Size = new System.Drawing.Size(1062, 176);
            this.txtMeddelande.TabIndex = 6;
            // 
            // webBrowser
            // 
            this.webBrowser.Location = new System.Drawing.Point(597, 388);
            this.webBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.Size = new System.Drawing.Size(587, 281);
            this.webBrowser.TabIndex = 7;
            // 
            // dataGridView
            // 
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Rubrik,
            this.Avsändare,
            this.DateTime});
            this.dataGridView.Location = new System.Drawing.Point(21, 388);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowTemplate.Height = 24;
            this.dataGridView.Size = new System.Drawing.Size(570, 281);
            this.dataGridView.TabIndex = 8;
            this.dataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellContentClick);
            // 
            // Rubrik
            // 
            this.Rubrik.DataPropertyName = "Rubrik";
            this.Rubrik.HeaderText = "Rubrik";
            this.Rubrik.Name = "Rubrik";
            // 
            // Avsändare
            // 
            this.Avsändare.DataPropertyName = "Avsändare";
            this.Avsändare.HeaderText = "Avsändare";
            this.Avsändare.Name = "Avsändare";
            // 
            // DateTime
            // 
            this.DateTime.DataPropertyName = "DateTime";
            this.DateTime.HeaderText = "DateTime";
            this.DateTime.Name = "DateTime";
            // 
            // btnReceive
            // 
            this.btnReceive.Location = new System.Drawing.Point(1091, 686);
            this.btnReceive.Name = "btnReceive";
            this.btnReceive.Size = new System.Drawing.Size(93, 23);
            this.btnReceive.TabIndex = 9;
            this.btnReceive.Text = "Receive";
            this.btnReceive.UseVisualStyleBackColor = true;
            // 
            // Email
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1196, 770);
            this.Controls.Add(this.btnReceive);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.webBrowser);
            this.Controls.Add(this.txtMeddelande);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtRubruk);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtTill);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSkicka);
            this.Name = "Email";
            this.Text = "Email";
            this.Load += new System.EventHandler(this.Email_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSkicka;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTill;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtRubruk;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtMeddelande;
        private System.Windows.Forms.WebBrowser webBrowser;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rubrik;
        private System.Windows.Forms.DataGridViewTextBoxColumn Avsändare;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateTime;
        private System.Windows.Forms.Button btnReceive;
    }
}