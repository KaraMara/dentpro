﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DentaProv1
{
    public partial class Tandläkare : Form
    {
        public Tandläkare()
        {
            InitializeComponent();
        }

        /*
        public void setLabelText(string value)
        {
            lblVälkommenTandläkare.Text = value;
        }
        */

        private void Tandläkare_Load(object sender, EventArgs e)
        {

        }

        private void lblVälkommenTandläkare_Click(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void läggTillNyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tandläkare_AddNewPatient form = new Tandläkare_AddNewPatient();
            form.ShowDialog();
            form.label3.Text = label3.Text;
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            this.Close();
            FirstSide first = new FirstSide();
            first.Show();
        }

        private void taBortToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ÄndraVy edit = new ÄndraVy();
            edit.ShowDialog();
            edit.label1.Text = label3.Text;
        }

        private void sökToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tandläkare_SearchPatient search = new Tandläkare_SearchPatient();
            search.ShowDialog();
            search.label3.Text = label3.Text;
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }
    }
}
